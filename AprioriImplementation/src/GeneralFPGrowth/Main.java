/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GeneralFPGrowth;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Kamran
 */
public class Main {

    static int threshold = 50;
    //static String file = "adult_test.data";F:\STCOP_data_directory\transaction.txt
    static String file = "F:\\FIM_data_directory\\T10I4D100K.txt";


    public static void main(String[] args) throws FileNotFoundException {
        long start = System.currentTimeMillis();
        System.out.println("Program started...");
        new FPGrowth(new File(file), threshold);
        System.out.println((System.currentTimeMillis() - start));
    }
}
