package ParallelImplementation;

import java.sql.*;
import java.util.*;


public class ThreadsUtilities {
	public static ArrayList<ArrayList<Integer>> powerSet(ArrayList<Integer> originalSet) {
		ArrayList<ArrayList<Integer>> sets = new ArrayList<ArrayList<Integer>>();
	    if (originalSet.isEmpty()) {
	        sets.add(new ArrayList<Integer>());
	        return sets;
	    }
	    List<Integer> list = new ArrayList<Integer>(originalSet);
	    Integer head = list.get(0);
	    ArrayList<Integer> rest = new ArrayList<Integer>(list.subList(1, list.size()));
	    for (ArrayList<Integer> set : powerSet(rest)) {
	    	ArrayList<Integer> newSet = new ArrayList<Integer>();
	        newSet.add(head);
	        newSet.addAll(set);
	        sets.add(newSet);
	        sets.add(set);
	    }
	    return sets;
	}
	public static ArrayList<ArrayList<Integer>> selfjoinWithLists(ArrayList<ArrayList<Integer>> x, ArrayList<ArrayList<Integer>> y, int individualListSize, ArrayList<ArrayList<Integer>> allFrequentPatterns)
	{
		ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
		int i, j, k;
		boolean flag;
		
		//System.out.println("inside self join function");
		//System.out.println(x);
		for(i=0;i<x.size();i++)
			for(j=0;j<y.size();j++)
			{
				//System.out.println(x.get(i));
				flag = true;
				for(k=0;k<individualListSize-1;k++)
				{
					if((int)x.get(i).get(k)==(int)y.get(j).get(k))
						flag = true;
					else
					{
						flag = false;
						break;
					}
				}
				if(flag == true && (int)x.get(i).get(individualListSize-1) < (int)y.get(j).get(individualListSize-1))
				{
					ArrayList<Integer> list1 = new ArrayList<Integer>();
					list1.addAll(x.get(i));
					//System.out.println("list1: "+list1);
					Integer lastElementofList2 = y.get(j).get(individualListSize-1);
					//System.out.println("addeee:"+lastElementofList2);
					list1.add(lastElementofList2);
					//System.out.println("x.get(i)"+x.get(i));
					result.add(list1);
					//list1.to
				}
			}
		if(individualListSize > 1)
		{
			ArrayList<ArrayList<Integer>> resultTemp = new ArrayList<ArrayList<Integer>>();
			resultTemp.addAll(result);
			Iterator<ArrayList<Integer>> itResult = result.iterator();
			while(itResult.hasNext())
			{
				ArrayList<Integer> resultVal = itResult.next();
				ArrayList<Integer> resultTempVal = resultTemp.get(resultTemp.indexOf(resultVal));
				ArrayList<ArrayList<Integer>> ss = new ArrayList<ArrayList<Integer>>();
				ss = powerSet(resultTempVal);
				Iterator<ArrayList<Integer>> itSS = ss.iterator();
				while(itSS.hasNext())
				{
					ArrayList<Integer> ssElement = itSS.next();
					//System.out.println(ssElement);
					if(ssElement.size() == individualListSize)
					{
						if(!allFrequentPatterns.contains(ssElement))
						{
							//System.out.println("Deleted: "+resultVal);
							resultTemp.remove(resultVal);
							break;
						}
					}
					else 
						continue;
				}
			}
			result = resultTemp;
			
		}
		return result;
	}
	public static void loadDataInDBTableFromFile(String fileName)
	{
		try{
			Connection con = null;
			Statement st = null;
			String sql;
			con = ThreadsDBConnection.getConnection();
			st = con.createStatement();
			sql = "DROP TABLE IF EXISTS transactions;";
			st.execute(sql);
			sql = "CREATE TABLE transactions (itemStr TEXT, itemsets INTEGER[]);";
			st.execute(sql);
			sql = "COPY transactions(itemStr) FROM '"  + ThreadsConstants.FILE_PATH + fileName +  "';";
			System.out.println(sql);
			st.execute(sql);
			sql = "UPDATE transactions SET itemsets = STRING_TO_ARRAY(itemStr, ' ', '')::INTEGER[];";
			st.execute(sql);
			sql = "ALTER TABLE transactions DROP COLUMN itemStr;";
			st.execute(sql);
			st.close();
		}catch(Exception e)
		{
			System.out.println("Error in load data");
			System.exit(0);
		}
	}
	
	public static ArrayList<ArrayList<Integer>> candidate_1_items_generation()
	{
		ArrayList<ArrayList<Integer>> c1 = new ArrayList<ArrayList<Integer>>();
		try{
			Connection con = null;
			Statement st = null;
			String sql;
			con = ThreadsDBConnection.getConnection();
			st = con.createStatement();
			ResultSet rs = null;
			sql = "SELECT DISTINCT unnest(itemsets) FROM transactions ORDER BY 1;";
			rs = st.executeQuery(sql);
			while(rs.next())
			{
				int c1Item = rs.getInt(1);
				ArrayList<Integer> singleItemList = new ArrayList<Integer>();
				singleItemList.add(c1Item);
				c1.add(singleItemList);
			}
			rs.close();
			st.close();
		}catch(Exception e)
		{
			System.out.println("Error in c1 gen");
			System.exit(0);
		}
		return c1;
	}
	public static ArrayList<ArrayList<Integer>> pruneBySupport(ArrayList<ArrayList<Integer>> candidateItemsets)
	{
		
		ArrayList<ArrayList<Integer>> frequentItemsets = new ArrayList<ArrayList<Integer>>();
		try{
			
			Iterator <ArrayList<Integer>> itCandidates = candidateItemsets.iterator();
			int listNum = 1;
			ArrayList<Thread> joinedThreads = new ArrayList<Thread>();
			while(itCandidates.hasNext())
			{
				ArrayList<Integer> currentCandidate = itCandidates.next();
				final int index = listNum;
				Thread t = new Thread( new Runnable()
						{
							
							@Override
							public void run()
							{
								try{
								System.out.println("Working on candidate set : "+currentCandidate);
								Connection con = ThreadsDBConnection.getConnection();
								String sql = "SELECT COUNT(*) FROM transactions WHERE itemsets @> ARRAY"+ currentCandidate + ";";
								//System.out.println(sql);
								ResultSet rs = null;
								Statement st = null;
								st = con.createStatement();
								rs = st.executeQuery(sql);
								rs.next();
								int supportOfCurrentItemset = rs.getInt(1);
								if(supportOfCurrentItemset >= ThreadsConstants.min_sup)
									frequentItemsets.add(currentCandidate);
								st.close();
								rs.close();
								//long randomSleep = (long)(Math.random() * 1000);
								//System.out.println("Thread "+ index + "sleep time : "+randomSleep);
								//Thread.sleep(randomSleep);
								}catch(Exception e)
								{
									System.out.println("Error in thread");
									e.printStackTrace();
								}
							}
						}
				);
				t.start();
				joinedThreads.add(t);
				listNum++;
			}
			
			
			for(Thread th: joinedThreads)
			{
				try{
					//System.out.println("Waiting for threads to finish");
					th.join();
					//System.out.println("Threads finished excuting");
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
		}catch(Exception e)
		{
			System.out.println("Error in pruning");
			e.printStackTrace();
			System.exit(0);
		}
		return frequentItemsets;
	}

}
