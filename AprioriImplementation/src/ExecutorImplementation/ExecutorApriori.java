package ExecutorImplementation;

import java.util.ArrayList;
import java.util.List;


public class ExecutorApriori {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				System.out.println("Executor Apriori begins...");
				ExecutorDBConnection.establishConnnection();
				
				String fileName = "5000i.txt";
				
				if(fileName.compareTo("5000i.txt") != 0)
				{
					ExecutorUtilities.loadDataInDBTableFromFile(fileName);
				}
				else
				{
					System.out.println("Load Comma Separted values...");
					ExecutorUtilities.loadCommaSeparatedData(fileName);
				}
				
				ArrayList<ArrayList<Integer>> candidateItemsets = new ArrayList<ArrayList<Integer>>();
				//List<ArrayList<Integer>> frequentItemsets = new ArrayList<ArrayList<Integer>>();
				List<ArrayList<Integer>> frequentItemsets = new ArrayList<ArrayList<Integer>>();
				ArrayList<ArrayList<Integer>> allFrequentItemsets = new ArrayList<ArrayList<Integer>>();
				
				long startTime = System.currentTimeMillis();
				
				//Candidate 1 itemsets
				candidateItemsets = ExecutorUtilities.candidate_1_items_generation();
				//System.out.println(candidateItemsets);
				//Frequent 1 itemsets
				frequentItemsets = ExecutorUtilities.pruneBySupport(candidateItemsets);
				System.out.println("Size 1 frequent itemsets: "+frequentItemsets.size());
				allFrequentItemsets.addAll(frequentItemsets);
				
				
				
				//candidateItemsets.clear();
				//candidateItemsets.addAll(frequentItemsets);
				int k = 2;
				while(!candidateItemsets.isEmpty())
				{
					candidateItemsets = ExecutorUtilities.selfjoinWithLists(frequentItemsets, frequentItemsets, k-1, allFrequentItemsets);
					if(candidateItemsets.isEmpty())
						break;
					frequentItemsets = ExecutorUtilities.pruneBySupport(candidateItemsets);
					System.out.println("Total Number of size "+k+" itemsets "+frequentItemsets.size());
					//System.out.println("Size "+k+" frequent itemsets: "+frequentItemsets);
					allFrequentItemsets.addAll(frequentItemsets);
					k = k + 1;
				}
				
				long endTime = System.currentTimeMillis();
				
				long programTime = endTime - startTime;
				
				System.out.println("Total No of frequent itemsets:"+allFrequentItemsets.size());
				System.out.println("All frequent itemset satisfies min_sup = "+ExecutorConstants.min_sup);
				//System.out.println(allFrequentItemsets);
				System.out.println("Program run time: "+ programTime);
				
				System.out.println("*********************************");
				ExecutorDBConnection.closeConnection();

	}

}
