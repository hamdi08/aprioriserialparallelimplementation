package SerialImplementatin;

import java.sql.*;  
import java.util.Properties;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/*
 * Written by: Shah Muhammad Hamdi
 * Date: 12/28/2015
 */

public class DBConnection {
	
	private static final String url_base = "jdbc:postgresql://";
	private static Connection general_connection = null;
	private static String url = null;
	private static Properties props = null;
	
	public static void establishConnnection()
	{
		props = showDbConnectionPane();
		try {
	         Class.forName("org.postgresql.Driver");
	         general_connection = DriverManager.getConnection(url, props);
	        
	      } catch (Exception e) {
	         e.printStackTrace();
	         System.out.println("Error in opening database by DBConnection.establishConnection");
	         System.err.println(e.getClass().getName()+": "+e.getMessage());
	         System.exit(0);
	      }
	      System.out.println("Database connection established successfully by DBConnection.establishConnection !");
	}
	/*
		 * creates a JOptionPane and asks for connection parameters
		 * it sets a property object, then sets the url for postgresql database	
		 * 
		 * @return properties object for db connection
	*/
		private static Properties showDbConnectionPane() {
			Properties props = new Properties();
			JTextField userName = new JTextField();
			//change if necessary
			userName.setText("postgres");
			JTextField dbUrl= new JTextField();
			dbUrl.setText("localhost");
			JTextField dbPort = new JTextField();
			dbPort.setText("5432");
			JPasswordField password = new JPasswordField();
			//change if necessary
			password.setText("1");
			JTextField dbName = new JTextField();
			//change if necessary
			dbName.setText("MarketBasketTransaction"); //TODO change this name! DONE
		    Object[] message = {"User Name:", userName, 
		    					"DbUrl:", dbUrl, 
		    					"DbPort:", dbPort, 
		    					"DbName:", dbName, 
		    					"Password:", password};//send text of filename
		    
		    int option = JOptionPane.showConfirmDialog(null, message, "DB Connection", JOptionPane.OK_CANCEL_OPTION);
		    
		    if(option == JOptionPane.OK_OPTION){
		    	props.setProperty("user", userName.getText());
				props.setProperty("password",new String(password.getPassword()) );
				//props.setProperty("ssl","true");
					
				url = url_base + dbUrl.getText() + ":" + dbPort.getText() + "/" + dbName.getText() +"";
				/*System.out.println(url);
		    	System.out.println(userName.getText());
			    System.out.println(dbUrl.getText());
			    System.out.println(dbPort.getText());
			    System.out.println(dbName.getText());
			    System.out.println(password.getPassword());*/
		    }
		    return props;
	}
	public static Connection getConnection()
	{
		try {
		do{
		if(!general_connection.isClosed()){
				return general_connection;
				} else {
				establishConnnection();
				return general_connection;
				}
			
		} while(general_connection.isClosed());
		} catch (SQLException e) {
		e.printStackTrace();
		System.err.println(e);
		}		
		return null;
	}
	public static void closeConnection()
	{
		try{
		general_connection.close();
		} catch(Exception e){
		e.printStackTrace();
		System.out.println("Error in closeConnection().");
	    System.err.println(e.getClass().getName()+": "+e.getMessage()); 
	    System.exit(0);
		}
	}
	

}
