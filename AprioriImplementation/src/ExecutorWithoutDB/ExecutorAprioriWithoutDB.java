package ExecutorWithoutDB;

import java.util.*;

/*
 * This program is takes two inputs as arguments data file path and min sup
 */

public class ExecutorAprioriWithoutDB {
	
	public static Set<Integer> distinctItems = new TreeSet<Integer>();
	public static ArrayList<ArrayList<Integer>> allTransactions = new ArrayList<ArrayList<Integer>>();
	public static int no_of_threads;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				System.out.println("Executor Apriori Without DB begins...");
				//ExecutorDBConnection.establishConnnection();
				
				String filePath = args[0];
				double min_sup = Double.parseDouble(args[1]); 
				
				ExecutorUtilitiesWithoutDB.loadDataInArrayList(filePath);
				//System.out.println("Distinct items: "+distinctItems);
				//System.out.println("All transactions: "+ allTransactions);
				//OK
				
				ArrayList<ArrayList<Integer>> candidateItemsets = new ArrayList<ArrayList<Integer>>();
				List<ArrayList<Integer>> frequentItemsets = new ArrayList<ArrayList<Integer>>();
				ArrayList<ArrayList<Integer>> allFrequentItemsets = new ArrayList<ArrayList<Integer>>();
				
				long startTime = System.currentTimeMillis();
				
				//Candidate 1 itemsets
				candidateItemsets = ExecutorUtilitiesWithoutDB.candidate_1_items_generation();
				//System.out.println("Can 1 itemsets: "+candidateItemsets);
				//OK
				//Frequent 1 itemsets
				frequentItemsets = ExecutorUtilitiesWithoutDB.pruneBySupport(candidateItemsets, min_sup);
				System.out.println("Size 1 frequent itemsets: "+frequentItemsets.size());
				allFrequentItemsets.addAll(frequentItemsets);
				
				
				
				//candidateItemsets.clear();
				//candidateItemsets.addAll(frequentItemsets);
				int k = 2;
				while(!candidateItemsets.isEmpty())
				{
					candidateItemsets = ExecutorUtilitiesWithoutDB.selfjoinWithLists(frequentItemsets, frequentItemsets, k-1, allFrequentItemsets);
					if(candidateItemsets.isEmpty())
						break;
					frequentItemsets = ExecutorUtilitiesWithoutDB.pruneBySupport(candidateItemsets, min_sup);
					System.out.println("Total Number of size "+k+" itemsets "+frequentItemsets.size());
					//System.out.println("Size "+k+" frequent itemsets: "+frequentItemsets);
					allFrequentItemsets.addAll(frequentItemsets);
					k = k + 1;
				}
				
				long endTime = System.currentTimeMillis();
				
				long programTime = endTime - startTime;
				
				System.out.println("Total No of frequent itemsets:"+allFrequentItemsets.size());
				System.out.println("All frequent itemset satisfies min_sup = "+ min_sup);
				//System.out.println(allFrequentItemsets);
				System.out.println("Number of threads: "+ no_of_threads);
				System.out.println("Program run time: "+ programTime);
				
				System.out.println("*********************************");
				//ExecutorDBConnection.closeConnection();

	}

}
