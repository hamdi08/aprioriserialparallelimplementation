package ParallelImplementation;

import java.util.*;

public class ThreadsAprioriOnMarketBasketData {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ThreadsDBConnection.establishConnnection();
		
		String fileName = "HansBookExample.txt";
		
		ThreadsUtilities.loadDataInDBTableFromFile(fileName);
		
		ArrayList<ArrayList<Integer>> candidateItemsets = new ArrayList<ArrayList<Integer>>();
		ArrayList<ArrayList<Integer>> frequentItemsets = new ArrayList<ArrayList<Integer>>();
		ArrayList<ArrayList<Integer>> allFrequentItemsets = new ArrayList<ArrayList<Integer>>();
		
		long startTime = System.currentTimeMillis();
		
		//Candidate 1 itemsets
		candidateItemsets = ThreadsUtilities.candidate_1_items_generation();
		System.out.println(candidateItemsets);
		//Frequent 1 itemsets
		frequentItemsets = ThreadsUtilities.pruneBySupport(candidateItemsets);
		System.out.println(frequentItemsets);
		allFrequentItemsets.addAll(frequentItemsets);
		
		
		
		//candidateItemsets.clear();
		//candidateItemsets.addAll(frequentItemsets);
		int k = 2;
		while(!candidateItemsets.isEmpty())
		{
			candidateItemsets = ThreadsUtilities.selfjoinWithLists(frequentItemsets, frequentItemsets, k-1, allFrequentItemsets);
			frequentItemsets = ThreadsUtilities.pruneBySupport(candidateItemsets);
			System.out.println("Total Number of size "+k+" itemsets "+frequentItemsets.size());
			System.out.println("Size "+k+" frequent itemsets: "+frequentItemsets);
			allFrequentItemsets.addAll(frequentItemsets);
			k = k + 1;
		}
		
		long endTime = System.currentTimeMillis();
		
		long programTime = endTime - startTime;
		
		System.out.println("Total No of frequent itemsets:"+allFrequentItemsets.size());
		System.out.println("All frequent itemset satisfying min_sup = "+ThreadsConstants.min_sup);
		System.out.println(allFrequentItemsets);
		System.out.println("Program run time: "+ programTime);
		
		System.out.println("*********************************");
		//ThreadsDBConnection.closeConnection();
		

	}

}
