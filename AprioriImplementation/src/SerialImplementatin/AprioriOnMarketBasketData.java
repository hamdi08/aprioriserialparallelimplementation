package SerialImplementatin;

import java.util.*;


public class AprioriOnMarketBasketData {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DBConnection.establishConnnection();
		
		String fileName = "chess.txt";
		
		Utilities.loadDataInDBTableFromFile(fileName);
		
		ArrayList<ArrayList<Integer>> candidateItemsets = new ArrayList<ArrayList<Integer>>();
		ArrayList<ArrayList<Integer>> frequentItemsets = new ArrayList<ArrayList<Integer>>();
		ArrayList<ArrayList<Integer>> allFrequentItemsets = new ArrayList<ArrayList<Integer>>();
		
		long startTime = System.currentTimeMillis();
		
		//Candidate 1 itemsets
		candidateItemsets = Utilities.candidate_1_items_generation();
		//System.out.println(candidateItemsets);
		//Frequent 1 itemsets
		frequentItemsets = Utilities.pruneBySupport(candidateItemsets);
		System.out.println("No. of size 1 frequent itemsets: "+frequentItemsets.size());
		//System.out.println(frequentItemsets);
		allFrequentItemsets.addAll(frequentItemsets);
		
		//candidateItemsets.clear();
		//candidateItemsets.addAll(frequentItemsets);
		int k = 2;
		while(!candidateItemsets.isEmpty())
		{
			candidateItemsets = Utilities.selfjoinWithLists(frequentItemsets, frequentItemsets, k-1, allFrequentItemsets);
			if (candidateItemsets.isEmpty())
				break;
			frequentItemsets = Utilities.pruneBySupport(candidateItemsets);
			System.out.println("No. of size "+k+" frequent itemsets: "+frequentItemsets.size());
			//System.out.println("Size "+k+" frequent itemsets: "+frequentItemsets);
			allFrequentItemsets.addAll(frequentItemsets);
			k = k + 1;
		}
		
		long endTime = System.currentTimeMillis();
		
		long programTime = endTime - startTime;
		
		System.out.println("No. of total frequent itemsets: "+ allFrequentItemsets.size());
		//System.out.println("All frequent itemset satisfying min_sup = "+Constants.min_sup);
		//System.out.println(allFrequentItemsets);
		System.out.println("Program run time: "+ programTime);
		
		DBConnection.closeConnection();
		

	}

}
