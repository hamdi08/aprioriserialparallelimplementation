package ExecutorImplementation;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.*;
import java.io.*;



public class ExecutorUtilities {
	public static ArrayList<ArrayList<Integer>> powerSet(ArrayList<Integer> originalSet) {
		ArrayList<ArrayList<Integer>> sets = new ArrayList<ArrayList<Integer>>();
	    if (originalSet.isEmpty()) {
	        sets.add(new ArrayList<Integer>());
	        return sets;
	    }
	    List<Integer> list = new ArrayList<Integer>(originalSet);
	    Integer head = list.get(0);
	    ArrayList<Integer> rest = new ArrayList<Integer>(list.subList(1, list.size()));
	    for (ArrayList<Integer> set : powerSet(rest)) {
	    	ArrayList<Integer> newSet = new ArrayList<Integer>();
	        newSet.add(head);
	        newSet.addAll(set);
	        sets.add(newSet);
	        sets.add(set);
	    }
	    return sets;
	}
	public static ArrayList<ArrayList<Integer>> selfjoinWithLists(List<ArrayList<Integer>> x, List<ArrayList<Integer>> y, int individualListSize, ArrayList<ArrayList<Integer>> allFrequentPatterns)
	{
		ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
		int i, j, k;
		boolean flag;
		
		//System.out.println("inside self join function");
		//System.out.println(x);
		for(i=0;i<x.size();i++)
			for(j=0;j<y.size();j++)
			{
				//System.out.println(x.get(i));
				flag = true;
				for(k=0;k<individualListSize-1;k++)
				{
					if((int)x.get(i).get(k)==(int)y.get(j).get(k))
						flag = true;
					else
					{
						flag = false;
						break;
					}
				}
				if(flag == true && (int)x.get(i).get(individualListSize-1) < (int)y.get(j).get(individualListSize-1))
				{
					ArrayList<Integer> list1 = new ArrayList<Integer>();
					list1.addAll(x.get(i));
					//System.out.println("list1: "+list1);
					Integer lastElementofList2 = y.get(j).get(individualListSize-1);
					//System.out.println("addeee:"+lastElementofList2);
					list1.add(lastElementofList2);
					//System.out.println("x.get(i)"+x.get(i));
					result.add(list1);
					//list1.to
				}
			}
		if(individualListSize > 1)
		{
			ArrayList<ArrayList<Integer>> resultTemp = new ArrayList<ArrayList<Integer>>();
			resultTemp.addAll(result);
			Iterator<ArrayList<Integer>> itResult = result.iterator();
			while(itResult.hasNext())
			{
				ArrayList<Integer> resultVal = itResult.next();
				ArrayList<Integer> resultTempVal = resultTemp.get(resultTemp.indexOf(resultVal));
				ArrayList<ArrayList<Integer>> ss = new ArrayList<ArrayList<Integer>>();
				ss = powerSet(resultTempVal);
				Iterator<ArrayList<Integer>> itSS = ss.iterator();
				while(itSS.hasNext())
				{
					ArrayList<Integer> ssElement = itSS.next();
					//System.out.println(ssElement);
					if(ssElement.size() == individualListSize)
					{
						if(!allFrequentPatterns.contains(ssElement))
						{
							//System.out.println("Deleted: "+resultVal);
							resultTemp.remove(resultVal);
							break;
						}
					}
					else 
						continue;
				}
			}
			result = resultTemp;
			
		}
		return result;
	}
	public static void loadDataInDBTableFromFile(String fileName)
	{
		try{
			Connection con = null;
			Statement st = null;
			String sql;
			con = ExecutorDBConnection.getConnection();
			st = con.createStatement();
			sql = "DROP TABLE IF EXISTS transactions;";
			st.execute(sql);
			sql = "CREATE TABLE transactions (itemStr TEXT, itemsets INTEGER[]);";
			st.execute(sql);
			sql = "COPY transactions(itemStr) FROM '"  + ExecutorConstants.FILE_PATH + fileName +  "';";
			System.out.println(sql);
			st.execute(sql);
			sql = "UPDATE transactions SET itemsets = STRING_TO_ARRAY(itemStr, ' ', '')::INTEGER[];";
			st.execute(sql);
			sql = "ALTER TABLE transactions DROP COLUMN itemStr;";
			st.execute(sql);
			st.close();
		}catch(Exception e)
		{
			System.out.println("Error in load data");
			System.exit(0);
		}
	}
	
	public static void loadCommaSeparatedData(String fileName)
	{
		try{
			Connection con = null;
			Statement st = null;
			String sql;
			con = ExecutorDBConnection.getConnection();
			st = con.createStatement();
			sql = "DROP TABLE IF EXISTS transactions;";
			st.execute(sql);
			sql = "CREATE TABLE transactions (itemsets INTEGER[]);";
			st.execute(sql);
			String filePath = ExecutorConstants.FILE_PATH + fileName;
			File f = new File(filePath);
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line;
			while((line = br.readLine())!=null)
			{
				String vals[] = line.split(", ");
				int itemArr[] = new int[vals.length-1];
				for(int i=0;i<vals.length-1;i++)
					itemArr[i] = Integer.parseInt(vals[i+1]);
				//System.out.println(Arrays.toString(itemArr));
				sql = "INSERT INTO transactions(itemsets) VALUES(ARRAY"+Arrays.toString(itemArr)+");";
				//System.out.println(sql);
				st.execute(sql);
			}
			st.close();
			br.close();
		}catch(Exception e)
		{
			System.out.println("Error in load data");
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	public static ArrayList<ArrayList<Integer>> candidate_1_items_generation()
	{
		ArrayList<ArrayList<Integer>> c1 = new ArrayList<ArrayList<Integer>>();
		try{
			Connection con = null;
			Statement st = null;
			String sql;
			con = ExecutorDBConnection.getConnection();
			st = con.createStatement();
			ResultSet rs = null;
			sql = "SELECT DISTINCT unnest(itemsets) FROM transactions ORDER BY 1;";
			rs = st.executeQuery(sql);
			while(rs.next())
			{
				int c1Item = rs.getInt(1);
				ArrayList<Integer> singleItemList = new ArrayList<Integer>();
				singleItemList.add(c1Item);
				c1.add(singleItemList);
			}
			rs.close();
			st.close();
		}catch(Exception e)
		{
			System.out.println("Error in c1 gen");
			System.exit(0);
		}
		return c1;
	}
	public static List<ArrayList<Integer>> pruneBySupport(ArrayList<ArrayList<Integer>> candidateItemsets)
	{
		
		List<ArrayList<Integer>> frequentItemsets = new ArrayList<ArrayList<Integer>> ();
		//Describe what will happen after each thread is started
		//Each thread represents one Callable object in the arraylist of callables
		//Each Callable object, i.e. the thread gets one element candidate set from the arraylist candidateItemsets
		//returns that candidate if it satisfies support otherwise null
		ArrayList<Callable<ArrayList<Integer>>> callables = new ArrayList<Callable<ArrayList<Integer>>>();
		candidateItemsets.stream().forEach( (currentCandiate) ->
		{
			callables.add(  new Callable<ArrayList<Integer>>()
			{
				@Override
				public ArrayList<Integer> call() throws Exception
				{
					//System.out.println("Thread invoking for "+currentCandiate);
					int supportOfCurrentItemset=0;
					try
					{
						Connection con = ExecutorDBConnection.getConnection();
						String sql = "SELECT COUNT(*) FROM transactions WHERE itemsets @> ARRAY"+ currentCandiate + ";";
						ResultSet rs = null;
						Statement st = null;
						st = con.createStatement();
						rs = st.executeQuery(sql);
						rs.next();
						supportOfCurrentItemset = rs.getInt(1);
					}catch(Exception e)
					{
						e.printStackTrace();
					}
					if(supportOfCurrentItemset >= ExecutorConstants.min_sup)
						return currentCandiate;
					else
						return null;
				}
			}
			);
		}
		);
		
		//Opens number of Threads as the same number of available processors
		System.out.println("No. of threads: " + Runtime.getRuntime().availableProcessors());
		ExecutorService service = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
		//ExecutorService service = Executors.newFixedThreadPool(10);
		
		try
		{
			List<Future<ArrayList<Integer>>> out = service.invokeAll(callables);
			out.stream().forEach( (frequent) ->
				{
					try {
						ArrayList<Integer> f = frequent.get();
						if(! f.isEmpty())
							frequentItemsets.add(f);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
					}
				}
			
			);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
		return frequentItemsets;
	}

}
